# Dotfiles

  Dotfiles are the hidden files in a Linux system. The name of the files (or folders) starts with a dot ('.') symbol, so they are termed as dotfiles. Generally they contain the configuration files of your system.

## My setup components :

1. bspwm
2. sxhkd
3. polybar
4. picom
5. termite (Terminal emulator)
6. rofi (Application Launcher)
7. ranger (Terminal based file manager)
8. neovim (Terminal based text editor)

## Screenshots :

![Screenshot-1](./images/my_desktop.png)
![Screenshot-1](./images/screenshot-2.png)
![Screenshot-1](./images/Screenshot-3.png)
## wallpaper :

![Wallpaper](https://gitlab.com/sourin0903/wallpapers/-/raw/master/0085.jpg)

You can grab [this](https://gitlab.com/sourin0903/wallpapers/-/blob/6ec4c267f34a9dfbefa91bd5ee98bd03e758d045/0085.jpg) wallpaper or check my wallpaper [repo](https://gitlab.com/sourin0903/wallpapers) for other wallpapers.